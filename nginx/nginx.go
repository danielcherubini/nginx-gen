package nginx

import (
	"io/ioutil"
	"strings"

	"github.com/danielcherubini/nginx-gen/models"
)

var baseConf = `server {
    listen 80;
    server_name <hostname>;
    #location ^~ /.well-known/acme-challenge/ {
    #    alias /usr/local/www/nginx/.well-known/acme-challenge/;
    #}
    location / {
        return 301 https://<hostname>$request_uri;
    }
}
server {
    listen 443 ssl;
    server_name <hostname> <ip>;
    # ssl_certificate /usr/local/etc/letsencrypt/live/<hostname>/fullchain.pem;
    # ssl_certificate_key /usr/local/etc/letsencrypt/live/<hostname>/privkey.pem;
    ssl_protocols TLSv1.2;
    ssl_prefer_server_ciphers on;
    ssl_ciphers "EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH";
    ssl_ecdh_curve secp384r1;
    ssl_session_cache shared:SSL:10m;
    ssl_session_tickets off;
    location / {
        proxy_pass  http://<ip>:<port>;
        proxy_set_header    X-Real-IP  $remote_addr;
        proxy_set_header    Host $host;
        proxy_set_header    X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header    X-Forwarded-Proto $scheme;
        proxy_redirect off;
    }
    access_log /var/log/nginx/<hostname>.log;
}`

// GenerateNginxConf takes hostname ip and port as strings and returns a config string
func GenerateNginxConf(server models.Server) string {
	str := baseConf
	str = strings.Replace(str, "<hostname>", server.Hostname, -1)
	str = strings.Replace(str, "<ip>", server.IP, -1)
	str = strings.Replace(str, "<port>", server.Port, -1)
	return str
}

// WriteNginxConf takes config as a string and filename as string and writes to file
func WriteNginxConf(config string, filename string) error {
	d1 := []byte(config)
	err := ioutil.WriteFile(filename, d1, 0644)
	if err != nil {
		return err
	}
	return nil
}
