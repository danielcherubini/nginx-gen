package main

import (
	"fmt"

	"github.com/BurntSushi/toml"
	"github.com/danielcherubini/nginx-gen/models"
	"github.com/danielcherubini/nginx-gen/nginx"
)

func main() {
	conf, err := openConfig("test.toml")
	if err != nil {
		fmt.Println("Error: ", err)
		return
	}
	for _, server := range conf.Servers {
		nginxConf := nginx.GenerateNginxConf(server)
		filename := conf.Nginx + "/" + server.Hostname
		err = nginx.WriteNginxConf(nginxConf, filename)
		if err != nil {
			fmt.Println("Error: ", err)
			return
		}
		fmt.Printf("Wrote Nginx Conf - %s\n", filename)
	}
}

func openConfig(filepath string) (models.Config, error) {
	var conf models.Config
	if _, err := toml.DecodeFile(filepath, &conf); err != nil {
		return conf, err
	}

	return conf, nil
}
