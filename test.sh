#!/bin/sh

HOSTNAME="foo"
IP="10.0.10.1"
PORT="8080"


read -r -d '' BASECONF << EOM
server {
    listen 80;
    server_name $HOSTNAME;
    location ^~ /.well-known/acme-challenge/ {
        alias /usr/local/www/nginx/.well-known/acme-challenge/;
    }
    location / {
        return 301 https://$HOSTNAME\$request_uri;
    }
}
server {
    listen 443 default_server ssl;
    server_name $HOSTNAME $IP;
    # ssl_certificate /usr/local/etc/letsencrypt/live/$HOSTNAME/fullchain.pem;
    # ssl_certificate_key /usr/local/etc/letsencrypt/live/$HOSTNAME/privkey.pem;
    ssl_protocols TLSv1.2;
    ssl_prefer_server_ciphers on;
    ssl_ciphers "EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH";
    ssl_ecdh_curve secp384r1;
    ssl_session_cache shared:SSL:10m;
    ssl_session_tickets off;
    location / {
        proxy_pass  http://$IP:$PORT;
        proxy_set_header    X-Real-IP  \$remote_addr;
        proxy_set_header    Host \$host;
        proxy_set_header    X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_set_header    X-Forwarded-Proto \$scheme;
        proxy_redirect off;
    }
    access_log /var/log/nginx/$HOSTNAME.log;
}
EOM



echo "$BASECONF"